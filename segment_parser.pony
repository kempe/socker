primitive SegmentLength
	fun string(): String val  => "SegmentLength"
primitive SegmentData
	fun string(): String val => "SegmentData"

type ParseState is (SegmentLength | SegmentData)

class SegmentParser
	var _segment_length: U32
	var _input_buffer: Array[U8] iso
	var _segment_buffer: Array[U8] iso

	var _parse_state: ParseState
	var _field_size: USize

	new create() =>
		_segment_length = 0
		_input_buffer = recover Array[U8] end
		_segment_buffer = recover Array[U8] end
		_parse_state = SegmentLength
		_field_size = 4

	fun ref change_state(to: ParseState) =>
		_parse_state = to
		match to
		| let s: SegmentLength =>
			_field_size = 4
			_segment_length = 0
		| let s: SegmentData =>
			_field_size = _segment_length.usize()
		end

	fun ref parse_segment(data: Array[U8] val): Array[U8] iso^ =>
		_input_buffer.reserve(_input_buffer.size() + data.size())
		for i in data.values() do
			_input_buffer.push(i)
		end

		var segment: Array[U8] iso = recover Array[U8] end
		match _parse_state
		| let s: SegmentLength =>
			try
				while (_field_size > 0) do
					_segment_length = _segment_length +
									  (_input_buffer.shift()?.u32() <<
									   (8 * (_field_size.u32() - 1)))
					_field_size = _field_size - 1

				end

				change_state(SegmentData)
				// If we still have data. Continue parsing.
				if _input_buffer.size() > 0 then
					segment = parse_segment(recover Array[U8] end)
				end
			end
		| let s: SegmentData =>
			try
				while _field_size > 0 do
					_segment_buffer.push(_input_buffer.shift()?)
					_field_size = _field_size - 1
				end

				change_state(SegmentLength)
				// Return _segment_buffer and allocate new buffer.
				segment = _segment_buffer = recover Array[U8] end
			end
		end

		consume segment

	fun create_segment(data: Array[U8] val): Array[U8] val =>
		let b: Array[U8] iso = recover Array[U8] end
		var i: USize = 0
		while i < 4 do
			b.push((data.size().u32() >> (8 * (4 - i - 1).u32())).u8())
			i = i + 1
		end
		b.copy_from(data, 0, 4, data.size())
		consume b
