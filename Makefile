CC ?= clang
CFLAGS ?=

.PHONY: clean
.SILENT: clean

socker: *.pony compressors/*.pony libsockserr.so libcompressor.so
	ponyc -p $(.CURDIR)

libsockserr.so: socks_error.c
	$(CC) -shared -fPIC $(CFLAGS) -o $(.TARGET) $(.ALLSRC)

libcompressor.so: compressors/zlib.c
	$(CC) -shared -fPIC -lz $(CFLAGS) -o $(.TARGET) $(.ALLSRC)

clean:
	- rm -f libsockserr.so libcompressor.so socker 2>&1
