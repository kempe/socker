use "compressors"
use "net"

primitive AuthNone
	fun apply(): U8 => 0x00

primitive AuthNotFound
	fun apply(): U8 => 0xFF

type SocksAuthMethod is (AuthNone | AuthNotFound)

class SocksConnectionRequest
	"""
	Represents the initial SOCKS5 connection request message. It
	contains three fields.

	+--------+-------------+------------------------------+
	| VER: 1 | NMETHODS: 1 | METHODS: NMETHODS (1 to 255) |
	+--------+-------------+------------------------------+

	VER is the version number and should always be 5.
	NMETHODS specifies the length of
	METHODS is an array of U8 values that indicates what
	authentication methads the client finds acceptable.
	"""

	let version: U8 = 5
	let method: SocksAuthMethod

	new create(data: Array[U8] val)? =>
		var method': SocksAuthMethod = AuthNotFound

		if data(0)? != version then
			error
		end

		// data(1) specifies the length of the method field.
		let nmethods = data(1)?

		if nmethods == 0 then
			error
		end

		// TODO: Support authentication
		var cnt: USize = 0
		repeat
			match data(cnt + 2)?
			| AuthNone() =>
				method' = AuthNone
				break
			end
			cnt = cnt + 1
		until cnt >= nmethods.usize()  end

		method = method'

class SocksMethodSelectionMessage
	"""
	Represents a authentication method selection message sent as a
	response to the initial connection request. It contains two
	fields.

	+--------+-----------+
	| VER: 1 | METHOD: 1 |
	+--------+-----------+

	VER is the version number and should always be 5.
	METHOD is the selected authentication method.
	"""

	let version: U8 = 5
	let method: SocksAuthMethod

	new create(method': SocksAuthMethod) =>
		method = method'

	fun serialise(): Array[U8] val =>
		var data: Array[U8] iso = recover Array[U8](2) end
		data.push(5)
		data.push(method())
		consume data

primitive SocksCmdConnect
	fun apply(): U8 => 0x01
primitive SocksCmdBind
	fun apply(): U8 => 0x02
primitive SocksCmdUDPAssociate
	fun apply(): U8 => 0x03

type SocksCommand is (SocksCmdConnect | SocksCmdBind |
                      SocksCmdUDPAssociate)

primitive SocksAddrIPv4
	fun apply(): U8 => 0x01
primitive SocksDomainName
	fun apply(): U8 => 0x03
primitive SocksAddrIPv6
	fun apply(): U8 => 0x04

type SocksAddressType is (SocksAddrIPv4 | SocksDomainName |
                      SocksAddrIPv6)

type SocksAddress is (U32 val | U128 val | String val)

class SocksAddressParser
	fun parse(atype: SocksAddressType,
	          offset: USize,
	          data: Array[U8] val): (SocksAddress, U16)? =>
		var address: SocksAddress = ""
		var port: U16 = 0
		match atype
		| let t: SocksAddrIPv4 =>
			var i: U32 = 0
			let ipv4_len: U32 = 4
			var address': U32 = 0
			while i < ipv4_len do
				address' = address' +
						  (data(offset + i.usize())?.u32() <<
				          (8 * (ipv4_len - i - 1)))
				i = i + 1
			end
			address = address'
			port = (data(offset + 4)?.u16() << 8) + data(offset + 5)?.u16()
		| let t: SocksAddrIPv6 =>
			var i: U128 = 0
			let ipv6_len: U128 = 16
			var address': U128 = 0
			while i < ipv6_len do
				address' = address' +
						  (data(offset + i.usize())?.u128() <<
				          (8 * (ipv6_len - i - 1)))
				i = i + 1
			end
			address = address'
			port = (data(offset + 16)?.u16() << 8) + data(offset + 17)?.u16()
		| let t: SocksDomainName =>
			var i: USize = 0
			let length: USize = data(offset)?.usize()
			var domain_name: Array[U8] iso =
				recover Array[U8](length) end

			while i < length do
				domain_name.push(data(offset + 1 + i)?)
				i = i + 1
			end

			address = String.from_array(consume domain_name)
			port = (data(offset + 1 + length)?.u16() << 8) +
			       data(offset + 1 + length + 1)?.u16()
		end
		(address, port)

class SocksRequestMessage
	"""
	Represents a SOCKS5 request. It is a request to establish a TCP
	connection, relay UDP traffic or listen for incoming TCP
	connections at/to the provided address and port.

	+--------+--------+--------+---------+--------------------+-------------+
	| VER: 1 | CMD: 1 | RSV: 1 | ATYP: 1 | DST.ADDR: Variable | DST.PORT: 2 |
	+--------+--------+--------+---------+--------------------+-------------+

	VER is the version number and should always be 5.
	CMD is the command to perform.
	RSV is reserved and should be set to 0x00.
	ATYP is the address type.
	DST.ADDR contains the destination address. It can be an IPv4 or
	         IPv6 address or a domain name.
	DST.PORT contains the destination port number.
	"""

	let version: U8 = 5
	let command: SocksCommand
	let address_type: SocksAddressType
	let destination_address: SocksAddress
	let destination_port: U16

	new create(data: Array[U8] val, out: OutStream)? =>
		// VER
		if data(0)? != version then
			error
		end

		// CMD
		match data(1)?
		| SocksCmdConnect() =>
			command = SocksCmdConnect
		| SocksCmdBind() =>
			command = SocksCmdBind
		| SocksCmdUDPAssociate() =>
			command = SocksCmdUDPAssociate
		else
			error
		end

		// ATYPE
		match data(3)?
		| SocksAddrIPv4() =>
			address_type = SocksAddrIPv4
		| SocksAddrIPv6() =>
			address_type = SocksAddrIPv6
		| SocksDomainName() =>
			address_type = SocksDomainName
		else
			error
		end

		(destination_address, destination_port) =
			SocksAddressParser.parse(address_type, 4, data)?

primitive SocksSucceeded
	fun apply(): U8 => 0x00
primitive SocksGeneralError
	fun apply(): U8 => 0x01
primitive SocksNotAllowed
	fun apply(): U8 => 0x02
primitive SocksNetworkUnreachable
	fun apply(): U8 => 0x03
primitive SocksHostUnreachable
	fun apply(): U8 => 0x04
primitive SocksConnectionRefused
	fun apply(): U8 => 0x05
primitive SocksTTLExpired
	fun apply(): U8 => 0x06
primitive SocksCommandNotSupported
	fun apply(): U8 => 0x07
primitive SocksAddressTypeNotSupported
	fun apply(): U8 => 0x08


type SocksError is (SocksSucceeded | SocksGeneralError |
                    SocksNotAllowed | SocksNetworkUnreachable |
                    SocksHostUnreachable | SocksConnectionRefused |
                    SocksTTLExpired | SocksCommandNotSupported |
                    SocksAddressTypeNotSupported)

class SocksErrors
	fun apply(value: U8) : SocksError? =>
		[
			SocksSucceeded
			SocksGeneralError
			SocksNotAllowed
			SocksNetworkUnreachable
			SocksHostUnreachable
			SocksConnectionRefused
			SocksTTLExpired
			SocksCommandNotSupported
			SocksAddressTypeNotSupported
		](value.usize())?

class SocksRequestReplyMessage
	"""
	Represents the reply to a SOCKS5 request.

	+--------+--------+--------+---------+--------------------+-------------+
	| VER: 1 | REP: 1 | RSV: 1 | ATYP: 1 | BND.ADDR: Variable | BND.PORT: 2 |
	+--------+--------+--------+---------+--------------------+-------------+

	VER is the version and shall always be 5.
	REP is the return code. One of SocksError.
	RSV is reserved and should be set to 0x00.
	ATYPE represents the address type; either IPv4, IPv6 or domain
		  name.
	BND.ADDR contains the local address on the socks server.
	BND.PORT contains the local port number on the socks server.
	"""

	let version: U8 = 5
	let reply: SocksError
	let address_type: SocksAddressType
	let bind_address: SocksAddress
	let bind_port: U16

	new create(reply': SocksError,
	           address_type': SocksAddressType,
	           bind_address': SocksAddress,
	           bind_port': U16) =>
		reply = reply'
		address_type = address_type'
		bind_address = bind_address'
		bind_port = bind_port'

	fun serialise(): Array[U8] val =>
		var msg: Array[U8] iso = recover Array[U8](4) end
		msg.push(version)
		msg.push(reply())
		msg.push(0x00)
		msg.push(address_type())

		match bind_address
		| let a: U32 =>
			var i: U8 = 4
			while i > 0 do
				msg.push((a >> (8 * (i.u32() - 1))).u8())
				i = i - 1
			end
		| let a: U128 =>
			var i: U8 = 16
			while i > 0 do
				msg.push((a >> (8 * (i.u128() - 1))).u8())
				i = i - 1
			end
		| let a: String val =>
			let a_arr: Array[U8] val = a.array()
			msg.copy_from(a_arr, 0, 4, a_arr.size())
		end

		msg.push((bind_port >> 8).u8())
		msg.push(bind_port.u8())

		consume msg

type SocksMessage is (SocksConnectionRequest |
                      SocksMethodSelectionMessage |
                      SocksRequestMessage)

primitive SocksUninitialised
primitive SocksAuthenticating
primitive SocksAuthenticated
primitive SocksForwarding

type SocksConnectionState is (SocksUninitialised | SocksAuthenticating |
                              SocksAuthenticated | SocksForwarding)

class SocksRelay
	let _out: OutStream
	let _connection: TCPConnection tag
	var _remote_connection: (TCPConnection tag | None)

	var _connection_state: SocksConnectionState = SocksUninitialised

	let _compressor: Compressor
	let _segment_parser: SegmentParser

	new create(out: OutStream, conn: TCPConnection tag) =>
		_out = out
		_connection = conn
		_remote_connection = None
		_compressor = Zlib(9)
		_segment_parser = SegmentParser

	fun ref _handle_segment(data: Array[U8] val): (SocksMessage val | None)? =>
		var decompressed: Array[U8] iso = recover Array[U8] end

		try
			decompressed = _compressor.decompress(data)?
		else
			_out.print("Failed to decompress incoming data")
			error
		end

		match _connection_state
		| SocksUninitialised =>
			// Request coming from client to SOCKS server
			let req = SocksConnectionRequest(consume decompressed)?
			_authenticate(req)?
			None
		| SocksAuthenticated =>
			let msg: SocksRequestMessage val =
				recover
					SocksRequestMessage(consume decompressed, _out)?
				end
			msg
		| SocksForwarding =>
			// Data coming from client to be forwarded to the end
			// point.
			try
				let r_conn = _remote_connection as TCPConnection
				r_conn.write(consume decompressed)
			end
			None
		else
			_out.print("SocksRelay: Unknown connection state")
		end

	fun ref parse(data: Array[U8] val): Array[SocksMessage val] val? =>
		var ret: Array[SocksMessage val] iso = recover Array[SocksMessage val] end
		// When the index is zero there is no data left in the buffer
		// and we have extracted it all.
		var buffer: Array[U8] val = _segment_parser.parse_segment(data)
		repeat
			if buffer.size() != 0 then
				let msg = _handle_segment(buffer)?
				match msg
				| None => None
				| let m: SocksRequestMessage val =>
					ret.push(m)
				end
			end
			buffer = _segment_parser.parse_segment(recover Array[U8] end)
		until buffer.size() == 0 end
		consume ret


	fun ref forward_to_local(data: Array[U8] val) =>
		try
			let d: Array[U8] val = _compressor.compress(data)?
			let segment: Array[U8] val = _segment_parser.create_segment(d)
			_connection.write(segment)
		else
			_out.print("Could not forward data to client. Compression failed.")
			return
		end


	fun ref _authenticate(req: SocksConnectionRequest)? =>
		match req.method
		| let m: AuthNone =>
			// Mark the client as authenticated and send response
			// message to the connection request as per the SOCKS5
			// specification.
			_connection_state = SocksAuthenticated
			_out.print("AuthNone chosen. Sending reply.")
			forward_to_local(SocksMethodSelectionMessage(m).serialise())
		else
			_out.print("No supported authentication method acceptable to client")
			forward_to_local(SocksMethodSelectionMessage(AuthNotFound).serialise())
			error
		end

	fun ref connected(conn: TCPConnection tag, local_address: NetAddress val,
	                  local_port: U16) =>
		_out.print("Connection opened")
		_remote_connection = conn
		_connection_state = SocksForwarding

		var addr_type: SocksAddressType = SocksAddrIPv4
		if local_address.ip6() then
			addr_type = SocksAddrIPv6
		end

		let addr: (U32 | U128) =
			match addr_type
			| let a: SocksAddrIPv4 =>
				local_address.ipv4_addr()
			| let a: SocksAddrIPv6 =>
				(let a1: U32, let a2: U32, let a3: U32, let a4: U32) =
					local_address.ipv6_addr()
				(a1.u128() + (a2.u128() << 32) + (a3.u128() << 64) +
				 (a4.u128() << 96))
			else
				_out.print("Invalid address type for socks connection")
				U32(0)
			end

		let msg = SocksRequestReplyMessage(SocksSucceeded, addr_type,
		                                   addr, local_port)
		forward_to_local(msg.serialise())

	fun ref connection_failed(err: U8) =>
		_out.print("Connection failed")
		try
			let msg = SocksRequestReplyMessage(SocksErrors(err)?, SocksAddrIPv4,
											   U32(0), 0)
			forward_to_local(msg.serialise())
		else
			_out.print("Could not send error message. Incorrect error
			           type: " + err.string())
		end
		kill()

	fun ref _reply_not_supported() =>
		_out.print("SOCKS command not supported")
		let msg = SocksRequestReplyMessage(SocksCommandNotSupported,
										   SocksAddrIPv4, U32(0), 0)
		forward_to_local(msg.serialise())
		kill()

	fun ref kill() =>
		_connection.dispose()
		try
			(_remote_connection as TCPConnection).dispose()
		end
