#include <errno.h>
#include <stdint.h>

enum {
	GENERAL_ERROR              = 0x01,
	CONNECTION_NOT_ALLOWED     = 0x02,
	NETWORK_UNREACHABLE        = 0x03,
	HOST_UNREACHABLE           = 0x04,
	CONNECTION_REFUSED         = 0x05,
	TTL_EXPIRED                = 0x06,
	COMMAND_NOT_SUPPORTED      = 0x07,
	ADDRESS_TYPE_NOT_SUPPORTED = 0x08
};

uint8_t get_socks_error(uint32_t err)
{
	switch (err)
	{
		case ENETUNREACH:
			return NETWORK_UNREACHABLE;
		case EHOSTUNREACH:
			return HOST_UNREACHABLE;
		case ECONNREFUSED:
			return CONNECTION_REFUSED;
		default:
			return GENERAL_ERROR;
	};
}
