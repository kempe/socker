interface Compressor
	fun compress(data: Array[U8] val): Array[U8] iso^?
	fun decompress(data: Array[U8] val): Array[U8] iso^?
