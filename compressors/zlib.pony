use "lib:compressor"


use @pony_deflateInit[Pointer[ZStream]](data: Pointer[U8] tag,
                                        size: U32, level: U8)

use @pony_compress[Pointer[U8]](z_stream: Pointer[ZStream],
                                o_size: Pointer[U32],
                                remaining: Pointer[U32])

use @pony_inflateInit[Pointer[ZStream]](data: Pointer[U8] tag, size: U32)

use @pony_decompress[Pointer[U8]](z_stream: Pointer[ZStream],
                                  o_size: Pointer[U32],
                                  remaining: Pointer[U32])

struct ZStream

class Zlib is Compressor
	let _level: U8
	let _chunk_size: U32 = 4000000

	new create(level: U8) =>
		_level = level

	fun compress(data: Array[U8] val): Array[U8] iso^? =>
		var z_stream: Pointer[ZStream]
		var compressed: Array[U8] iso = recover Array[U8] end
		z_stream = @pony_deflateInit(data.cpointer(), data.size().u32(), _level)
		if z_stream.is_null() then
			error
		end

		var remaining: U32 = 0
		repeat
			var o_size: U32 = _chunk_size
			let d = @pony_compress(z_stream, addressof o_size,
			                       addressof remaining)
			if d.is_null() then
				error
			end
			let buffer: Array[U8] = Array[U8].from_cpointer(d,
			                                                o_size.usize(),
			                                                _chunk_size.usize())
			for i in buffer.values() do
				compressed.push(i)
			end
		until remaining == 0 end

		consume compressed

	fun decompress(data: Array[U8] val): Array[U8] iso^? =>
		var z_stream: Pointer[ZStream]
		var decompressed: Array[U8] iso = recover Array[U8] end
		z_stream = @pony_inflateInit(data.cpointer(), data.size().u32())
		if z_stream.is_null() then
			error
		end

		var remaining: U32 = 0
		repeat
			var o_size: U32 = _chunk_size
			let d = @pony_decompress(z_stream, addressof o_size,
			                         addressof remaining)
			if d.is_null() then
				error
			end
			let buffer: Array[U8] = Array[U8].from_cpointer(d,
			                                                o_size.usize(),
			                                                _chunk_size.usize())
			for i in buffer.values() do
				decompressed.push(i)
			end
		until remaining == 0 end

		consume decompressed
