#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>

z_stream* pony_deflateInit(uint8_t* data, uint32_t size, uint32_t level)
{
	z_stream *strm = malloc(sizeof(z_stream));
	if (!strm)
		return NULL;

	strm->zalloc = Z_NULL;
	strm->zfree = Z_NULL;
	strm->opaque = Z_NULL;

	if (deflateInit(strm, level) != Z_OK)
		return NULL;

	strm->avail_in = size;
	strm->next_in = data;

	return strm;
}

uint8_t* pony_compress(z_stream *strm, uint32_t *o_size, uint32_t *remaining)
{
	int ret;
	uint8_t *buf = malloc(*o_size);
	if (!buf)
		goto clean;

	strm->avail_out = *o_size;
	strm->next_out = buf;

	if ((ret = deflate(strm, Z_FINISH)) == Z_STREAM_ERROR)
	{
		free(buf);
		buf = NULL;
		goto clean;
	}

	*o_size = *o_size - strm->avail_out;
	*remaining = strm->avail_in;

	if (ret == Z_STREAM_END)
		goto clean;

	goto out;

clean:
	deflateEnd(strm);
	free(strm);
out:
	return buf;
}

z_stream* pony_inflateInit(uint8_t* data, uint32_t size)
{
	z_stream *strm = malloc(sizeof(z_stream));
	if (!strm)
		return NULL;

	strm->zalloc = Z_NULL;
	strm->zfree = Z_NULL;
	strm->opaque = Z_NULL;

	if (inflateInit(strm) != Z_OK)
		return NULL;

	strm->avail_in = size;
	strm->next_in = data;

	return strm;
}

uint8_t* pony_decompress(z_stream *strm, uint32_t *o_size, uint32_t *remaining)
{
	int ret;
	uint8_t *buf = malloc(*o_size);
	if (!buf)
		goto clean;

	strm->avail_out = *o_size;
	strm->next_out = buf;

	if ((ret = inflate(strm, Z_NO_FLUSH)) == Z_STREAM_ERROR)
	{
		free(buf);
		buf = NULL;
		goto clean;
	}
	switch (ret) {
		case Z_NEED_DICT:
		case Z_DATA_ERROR:
		case Z_MEM_ERROR:
			buf = NULL;
			goto clean;
	}


	*o_size = *o_size - strm->avail_out;
	*remaining = strm->avail_in;

	if (ret == Z_STREAM_END)
		goto clean;

	goto out;

clean:
	inflateEnd(strm);
	free(strm);
out:
	return buf;
}
