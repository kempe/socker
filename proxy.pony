use "net"
use "lib:sockserr"

use @get_socks_error[U8](errno: U32)

class ProxyAcceptor is TCPListenNotify
	let _env: Env
	let _auth: TCPAuth

	new create(env: Env, auth: TCPAuth) =>
		_env = env
		_auth = auth

	fun ref connected(listen: TCPListener ref): TCPConnectionNotify iso^ =>
		let proxy = Proxy(_env.out, _auth)
		recover ProxyNotifier(_env.out, proxy) end

	fun ref not_listening(listen: TCPListener ref) =>
		_env.out.print("Stopped listening")

class SocksNotifier is TCPConnectionNotify
	let _proxy: Proxy tag

	new create(proxy: Proxy tag) =>
		_proxy = proxy

	fun ref connected(conn: TCPConnection ref) =>
		_proxy.socks_connected(conn, conn.local_address(),
		                       conn.local_address().port())

	fun ref connect_failed(conn: TCPConnection ref) =>
		(let ret: U32, let errno: U32) = conn.get_so_error()
		let err: U8 = @get_socks_error[U8](errno)
		_proxy.socks_failed(conn, err)

	fun ref closed(conn: TCPConnection ref) =>
		_proxy.kill(conn)

	fun ref received(conn: TCPConnection ref,
	                 data: Array[U8] iso,
	                 times: USize) : Bool =>
		_proxy.socks_relay(consume data)
		true

actor Proxy is ProxyInterface
	let _out: OutStream
	let _auth: TCPAuth

	var _socks_relay: (SocksRelay | None)

	new create(out: OutStream, auth: TCPAuth)	=>
		_out = out
		_auth = auth
		_socks_relay = None

	be accepted(conn: TCPConnection tag) =>
		_socks_relay = SocksRelay(_out, conn)

	be connected(conn: TCPConnection tag) =>
		_out.print("ERROR: connected called in proxy. Should not happen!")

	be socks_connected(conn: TCPConnection tag, local_address: NetAddress val,
	                   local_port: U16) =>
		try
			(_socks_relay as SocksRelay).connected(conn, local_address,
			                                       local_port)
		end

	be kill(caller: TCPConnection tag) =>
		try
			(_socks_relay as SocksRelay).kill()
			_socks_relay = None
		end

	fun ipv4_to_str(address: U32): String =>
		(address >> 24).string() + "." +
		((address >> 16) and 0xFF).string() + "." +
		((address >> 8) and 0xFF).string() + "." +
		(address and 0xFF).string()

	fun ipv6_to_str(address: U128): String =>
		var res: String = ""
		var i: U128 = 8
		while i > 1 do
			res = res + ((address >> (16 * (i - 1))) and 0xFFFF).string() + ":"
			i = i - 1
		end
		res = res + (address and 0xFFFF).string()
		res

	fun ref _new_connection(msg: SocksRequestMessage val) =>
		var address: String

		address =
			match msg.destination_address
			| let a: U32 =>
				ipv4_to_str(a)
			| let a: U128 =>
				ipv6_to_str(a)
			| let a: String =>
				a
			end

		match msg.command
		| let c: SocksCmdConnect =>
			_out.print("Opening connection to " + address + ":" +
			           msg.destination_port.string())
			TCPConnection(_auth, recover SocksNotifier(this) end,
						  address,
						  msg.destination_port.string())
		else
			try
				(_socks_relay as SocksRelay)._reply_not_supported()
			end
		end

	be relay(from: TCPConnection tag, data: Array[U8 val] val) =>
		try
			let r = _socks_relay as SocksRelay

			for msg in r.parse(data)?.values() do
				match msg
				| let req: SocksRequestMessage val =>
					_new_connection(req)
				end
			end
		else
			_out.print("Failed to parse data from local client")
		end

	be socks_relay(data: Array[U8 val] val) =>
		try
			let r = _socks_relay as SocksRelay
			r.forward_to_local(data)
		end

	be socks_failed(conn: TCPConnection tag, err: U8) =>
		try
			let r = _socks_relay as SocksRelay
			r.connection_failed(err)
		else
			_out.print("Connection failed, but no relay active!")
		end
