use "compressors"
use "net"

class ClientAcceptor is TCPListenNotify
	"""
	Accepts client connections and initiates a connection to the
	server for relaying.
	"""

	let _env: Env
	let _auth: AmbientAuth

	let _remote_address: String
	let _remote_port: String

	new create(env: Env, auth: AmbientAuth,
	           remote_address: String,
	           remote_port: String) =>
		_env = env
		_auth = auth
		_remote_address = remote_address
		_remote_port = remote_port

	fun ref connected(listen: TCPListener ref): TCPConnectionNotify iso^ =>
		let client = Client(_env.out)

		/*
		 * At this point, the local connection is established. Before
		 * returning the notifier, we initialise a connection to the
		 * remote server and create another notifier for that remote
		 * connection.
		 */
		TCPConnection(_auth, recover ProxyNotifier(_env.out, client) end,
		              _remote_address, _remote_port)

		recover ProxyNotifier(_env.out, client) end

	fun ref not_listening(listen: TCPListener ref) =>
		_env.out.print("No longer listening")

actor Client is ProxyInterface
	"""
	This class simply shuffles data from the locally connected client
	to the remote server. The connections are established by the TCP
	handling logic and the active connections are passed to this actor
	via connected() for the remote connection and accepted() for the
	local connection. Data arriving via the relay() callback will be
	passed between the two connections.
	"""

	let _out: OutStream

	var _local: (TCPConnection tag | None)
	var _remote: (TCPConnection tag | None)

	var _local_buffer: Array[U8]
	var _remote_buffer: Array[U8]

	let _compressor: Compressor
	let _segment_parser: SegmentParser

	new create(out: OutStream)	=>
		_out = out
		_local = None
		_remote = None
		_local_buffer = Array[U8]
		_remote_buffer = Array[U8]
		_compressor = Zlib(9)
		_segment_parser = SegmentParser

	fun _get_opposite(conn: TCPConnection tag) : TCPConnection tag? =>
		if _local is conn then
				_remote as TCPConnection
		else
				_local as TCPConnection
		end

	be accepted(conn: TCPConnection tag) =>
		_local = conn

		if _remote_buffer.size() > 0 then
			send_buffer(conn, _remote_buffer)
		end

	be connected(conn: TCPConnection tag) =>
		_remote = conn

		if _local_buffer.size() > 0 then
			send_buffer(conn, _local_buffer)
		end

	fun ref send_to_remote(data: Array[U8] val)? =>
		let remote = _remote as TCPConnection

		try
			let d: Array[U8] val = _compressor.compress(data)?
			let segment: Array[U8] val = _segment_parser.create_segment(d)
			remote.write(segment)
		else
			_out.print("Error compressing outgoing data.")
		end

	fun ref send_to_local(data: Array[U8] val)? =>
		let local = _local as TCPConnection

		var index: USize = 0
		var buffer: Array[U8] val =
			_segment_parser.parse_segment(data)
		repeat
			if buffer.size() != 0 then
				try
					local.write(_compressor.decompress(buffer)?)
				else
					_out.print("Error decompressing incoming data.")
				end
				buffer = _segment_parser.parse_segment(recover Array[U8] end)
			end
		until buffer.size() == 0 end

	fun ref send_buffer(conn: TCPConnection tag, buffer: Array[U8]) =>
		let bs = buffer.size()
		let data: Array[U8] iso = recover Array[U8](bs) end
		var compressed: Array[U8] iso = recover Array[U8] end

		for d in buffer.values() do
			data.push(d)
		end
		buffer.truncate(0)

		try
			if conn is _remote then
				send_to_remote(consume data)?
			elseif conn is _local then
				send_to_local(consume data)?
			end
		else
			_out.print("Error sending outgoing data.")
		end

	be kill(caller: TCPConnection tag) =>
		try
			// If this fails, the other connection was never opened.
			// Ignore the error.
			_get_opposite(caller)?.dispose()
		end

	be relay(from: TCPConnection tag, data: Array[U8 val] val) =>
		try
			/*
			 * It is possible that the opposite connection we're
			 * sending to is None. In that case the else clause of the
			 * try will buffer the data for later sending.
			 */
			if from is _local then
				send_to_remote(data)?
			elseif from is _remote then
				send_to_local(data)?
			end
		else
			/*
			 * If we couldn't get the opposite connection, that means
			 * one side still hasn't connected. We queue the data and
			 * send it when the connection is made.
			 */
			if from is _local then
				_local_buffer.concat(data.values())
			elseif from is _remote then
				_remote_buffer.concat(data.values())
			else
				_out.print("ERROR: Received data from connection " +
				           "that was neither local nor remote")
			end
		end
