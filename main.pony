use "cli"
use "logger"
use "net"

actor Main
	new create(env: Env) =>
		try
			var client: Bool

			let client_spec =
				CommandSpec.leaf("client",
                    """
				    Run as a client and connect to a socker server
				    available at address:port. The client will open a tcp
				    port at localhost:local_port that acts as a SOCKS5
				    proxy compressing and relaying data to the socker
				    server.
				    """
				                 , [
					OptionSpec.string("address", "Listen address"
					                  where short' = 'a', default' = "localhost")
					OptionSpec.string("port", "Listen port"
					                  where short' = 'p', default' = "8081")
					OptionSpec.string("remote_address", "Server address"
					                  where short' = 'r', default' = "localhost")
					OptionSpec.string("remote_port", "Server port"
					                  where short' = 's', default' = "8080")
				], [])?

			let server_spec =
				CommandSpec.leaf("server",
				    """
				    Run in server mode and listen for incoming connections
					at addr:port. Will act as a SOCKS5 proxy for incoming
				    traffic from a socker client.
				    """
				                 , [
					OptionSpec.string("address", "Listen address"
					                  where short' = 'a', default' = "localhost")
					OptionSpec.string("port", "Listen port"
					                  where short' = 'p', default' = "8080")
				], [])?

			let cs =
				CommandSpec.parent("socker", "A compressing SOCKS5 proxy",
				                   [], [server_spec; client_spec])?
				                   .> add_help()?

			var cmd =
				match CommandParser(cs).parse(env.args, env.vars)
				| let c: Command => c
				| let ch: CommandHelp =>
					ch.print_help(env.out)
					error
				| let se: SyntaxError =>
					env.out.print(se.string())
					error
				end

			if cmd.fullname() == "socker/client" then
				client = true
			else
				client = false
			end

			let address = cmd.option("address").string()
			let port = cmd.option("port").string()

			var acceptor =
				if client then
					let remote_address = cmd.option("remote_address").string()
					let remote_port = cmd.option("remote_port").string()

					env.out.print("Running as client")
					env.out.print("Listening on " + address + ":" +
					              port + "/tcp")
					env.out.print("Server at " + remote_address + ":"
					              + remote_port + "/tcp")
					recover iso
						ClientAcceptor(env,
						               env.root as AmbientAuth,
						               remote_address,
						               remote_port)
					end
				else
					env.out.print("Running as server")
					recover iso
						let auth = env.root as AmbientAuth
						ProxyAcceptor(env,
						              TCPAuth(auth))
					end
				end

			try
				TCPListener(env.root as AmbientAuth,
							consume acceptor,
							address, port)
			else
				env.out.print("Error listening on " + address + ":" +
				               port + "/tcp")
			end
		else
			env.out.print("General Error")
			env.exitcode(1)
			return
		end
