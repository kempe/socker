use "net"

interface ProxyInterface
	"""
	This interface is used by Notifiers for relaying data.
	The actor implementing this interface is expected to relay the
	data from the client to the server.

	When the local connection has been accepted, the ClientNotifier
	should call accepted().

	When the remote connection is established to the server, the
	ClientNotifier should call connected().
	"""

	// Notify relay that the remote connection has been established.
	be connected(conn: TCPConnection tag)
	// Notify the relay that the local connection has been accepted.
	be accepted(conn: TCPConnection tag)
	// Kill the relay. Generally done if a connection closes.
	be kill(caller: TCPConnection tag)
	// Relay data to the other end-point.
	be relay(from: TCPConnection tag, data: Array[U8 val] val)

class ProxyNotifier is TCPConnectionNotify
	"""
	This class passes data and events from the TCPConnection to the
	ProxyInterface actor that handles relaying data between connections.
	"""

	let _out: OutStream
	let _client: ProxyInterface tag

	new create(out: OutStream, client: ProxyInterface tag) =>
		_out = out
		_client = client

	fun ref accepted(conn: TCPConnection ref) =>
		_client.accepted(conn)

	fun ref connected(conn: TCPConnection ref) =>
		_client.connected(conn)

	fun ref closed(conn: TCPConnection ref) =>
		_client.kill(conn)

	fun ref received(conn: TCPConnection ref,
	                 data: Array[U8] iso,
	                 times: USize) : Bool =>
		_client.relay(conn, consume data)
		true

	fun ref connect_failed(conn: TCPConnection ref) =>
		_client.kill(conn)

